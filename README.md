# room-optimizer

Room Occupancy Optimization Tool - Smart Host Coding Challenge

###  Run the application

I included a maven wrapper so that we can run Maven without having it installed and present on the path. To Launch the application you can use either the maven wrapper (mvnw for Linux bash or mvnw.cmd for Windows environments):

```
./mvnw clean spring-boot:run
```

or run it as packaged application:
```
./mvnw clean package
```
```
./target/java –jar room-optimizer-0.0.1-SNAPSHOT.jar
```

When the app starts, we can immediately interrogate it.
```
curl -v http://localhost:8080/room-optimizer?freeEconomyRooms=3&freePremiumRooms=4
```
