package com.smarthost.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class RoomResultDto {
    private int usedEconomyRooms;
    private int economyRoomsProfit;
    private int usedPremiumRooms;
    private int premiumRoomsProfit;
}