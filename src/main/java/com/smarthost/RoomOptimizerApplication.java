package com.smarthost;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.util.List;

import static com.fasterxml.jackson.core.JsonParser.Feature.ALLOW_TRAILING_COMMA;

@SpringBootApplication
public class RoomOptimizerApplication {

	private static final String JSON_RESOURCE = "/smarthost_hotel_guests.json";

	public static void main(String[] args) {

		SpringApplication.run(RoomOptimizerApplication.class, args);
	}

	@Bean
	public List<Integer> potentialGuests() throws Exception {

		return new ObjectMapper()
				.configure(ALLOW_TRAILING_COMMA, true)
				.readValue(RoomOptimizerApplication.class.getResourceAsStream(JSON_RESOURCE), new TypeReference<List<Integer>>(){});

	}
}
