package com.smarthost.controller;


import com.smarthost.dto.RoomResultDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

import static java.util.Comparator.comparingInt;
import static java.util.stream.Collectors.partitioningBy;

@RestController
public class RoomOptimizerController {

    private List<Integer> premiumGuests;

    private List<Integer> economyGuests;

    public RoomOptimizerController(@Autowired final List<Integer> potentialGuests) {

        final Map<Boolean, List<Integer>> guestGroups = potentialGuests
                .stream()
                .sorted(comparingInt(x -> -x))
                .collect(partitioningBy(p -> p >= 100));

        this.premiumGuests = guestGroups.get(true);
        this.economyGuests = guestGroups.get(false);
    }

    @GetMapping("/room-optimizer")
    public ResponseEntity<RoomResultDto> checkOccupancy(@RequestParam(value = "freeEconomyRooms") final int freeEconomyRooms,
                                                        @RequestParam(value = "freePremiumRooms") final int freePremiumRooms) {

        int usedPremiumRooms = Math.min(freePremiumRooms, premiumGuests.size());

        int usedUpgradedRooms = Math.min(Math.max(freePremiumRooms - usedPremiumRooms, 0),
                Math.max(economyGuests.size() - freeEconomyRooms, 0));

        int usedEconomyRooms = Math.min(economyGuests.size() - usedUpgradedRooms, freeEconomyRooms);

        int economyRoomsProfit = economyGuests
                .stream()
                .skip(usedUpgradedRooms)
                .limit(usedEconomyRooms)
                .mapToInt(Integer::intValue)
                .sum();

        int premiumRoomsProfit = premiumGuests
                .stream()
                .limit(usedPremiumRooms)
                .mapToInt(Integer::intValue)
                .sum();

        int upgradedRoomsProfit = economyGuests
                .stream()
                .limit(usedUpgradedRooms)
                .mapToInt(Integer::intValue)
                .sum();


        return ResponseEntity.ok().body(
                new RoomResultDto(
                        usedEconomyRooms,
                        economyRoomsProfit,
                        usedPremiumRooms + usedUpgradedRooms,
                        premiumRoomsProfit + upgradedRoomsProfit)
        );
    }
}

