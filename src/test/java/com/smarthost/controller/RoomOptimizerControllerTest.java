package com.smarthost.controller;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.reactive.server.WebTestClient;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureWebTestClient
public class RoomOptimizerControllerTest {

    private final static String URI = "/room-optimizer?freeEconomyRooms={param1}&freePremiumRooms={param2}";

    @Autowired
    private WebTestClient webClient;

    @Test
    public void testThreeAvailableEconomyRoomsAndThreeAvailablePremiumRooms() {

        webClient.get().uri(URI,
                3, 3)
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.usedEconomyRooms").isEqualTo(3)
                .jsonPath("$.economyRoomsProfit").isEqualTo(167)
                .jsonPath("$.usedPremiumRooms").isEqualTo(3)
                .jsonPath("$.premiumRoomsProfit").isEqualTo(738);
    }

    @Test
    public void testFiveAvailableEconomyRoomsAndSevenAvailablePremiumRooms() {

        webClient.get().uri(URI,
                5, 7)
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.usedEconomyRooms").isEqualTo(4)
                .jsonPath("$.economyRoomsProfit").isEqualTo(189)
                .jsonPath("$.usedPremiumRooms").isEqualTo(6)
                .jsonPath("$.premiumRoomsProfit").isEqualTo(1054);
    }
    @Test
    public void testSevenAvailableEconomyRoomsAndTwoAvailablePremiumRooms() {

        webClient.get().uri(URI,
                7, 2)
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.usedEconomyRooms").isEqualTo(4)
                .jsonPath("$.economyRoomsProfit").isEqualTo(189)
                .jsonPath("$.usedPremiumRooms").isEqualTo(2)
                .jsonPath("$.premiumRoomsProfit").isEqualTo(583);
    }

    @Test
    public void testOneAvailableEconomyRoomsAndSevenAvailablePremiumRooms() {

        webClient.get().uri(URI,
                1, 7)
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.usedEconomyRooms").isEqualTo(1)
                .jsonPath("$.economyRoomsProfit").isEqualTo(45)
                .jsonPath("$.usedPremiumRooms").isEqualTo(7)
                .jsonPath("$.premiumRoomsProfit").isEqualTo(1153);
    }


}